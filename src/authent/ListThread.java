package authent;

import java.util.ArrayList;

public class ListThread {
	private static ArrayList<Thread> listThread;
	
	public ListThread() {
		listThread = new ArrayList<Thread>();
	}

	public static ArrayList<Thread> getListThread() {
		return listThread;
	}

	public static void setThread(Thread t) {
		listThread.add(t);
	}
	
	public static Thread getThread(int i) {
		return listThread.get(i);
	}
	
	public static void removeAll() {
		for(int i=0;i<listThread.size();i++) {
			listThread.remove(i);
		}
	}
	
	public static void afficher() {
		for(int i=0;i<listThread.size();i++) {
			System.out.println(listThread.get(i));
		}
	}
}
