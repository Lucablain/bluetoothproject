package authent;

public class Device {
	
	private String imac;
	private String email;
	private String num;
	
	
	public Device (String mac , String mail, String num) {
		this.imac = mac;
		this.email = mail;
		this.num = num;
	}

	public String getImac() {
		return imac;
	}

	public void setImac(String imac) {
		this.imac = imac;
	}

	public String getMail() {
		return email;
	}

	public void setMail(String mail) {
		this.email = mail;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}
	
	

}
