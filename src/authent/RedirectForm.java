package authent;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class RedirectForm extends HttpServlet {
      private static final long serialVersionUID = 1L;
 
      public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      }
 
      public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            // R�cup�rer les donn�es re�ues du formulaire
            String nameTel = (String) request.getParameter("nameTel");
            String adressMAC = (String) request.getParameter("adressMAC");
            System.out.println(nameTel);
            
            
            // Si l'un des champs est vide
            if ("".equals(nameTel)) {
                  request.setAttribute("erreur", "Un champ est en erreur");
                  // Redirection vers le formulaire form.jsp
                  getServletContext().getRequestDispatcher("/listNewClient.jsp")
                             .forward(request, response);
            }
 
            // Sinon
            else {
                  request.setAttribute("nameTel", nameTel);
                  request.setAttribute("adressMAC", adressMAC);
              	try {
					Thread.sleep(1000);
	                  //Redirection vers la page hello.jsp
	                  getServletContext().getRequestDispatcher("/formAjoutClient.jsp")
	                             .forward(request, response);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
              	
            }
      }
}
 