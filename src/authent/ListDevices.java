package authent;

import java.util.ArrayList;

public class ListDevices {
	
	public ArrayList<Device> devices;
	
	public ListDevices() {
		devices = new ArrayList<Device>();
	}

	public ArrayList<Device> getDevices() {
		return devices;
	}

	public void setDevices(Device device) {
		System.out.println(device);
		devices.add(device);
	}
	
	public Device getRD(String iMac) {
		for(int i=0;i<devices.size();i++) {
			if(devices.get(i).getImac().equals(iMac)) {
				return devices.get(i);
			}
		}
		return null;
	}

}
