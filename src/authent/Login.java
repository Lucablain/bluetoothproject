package authent;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// R�cup�rer les donn�es re�ues du formulaire

		
		Properties properties = new Properties();
		try	{
			properties.load(Login.class.getClassLoader().getResourceAsStream("/application.properties"));
		}
		finally {
			System.out.println("Probleme load Properties");
		}
		String  user = properties.getProperty("user");
        String password = properties.getProperty("password");

		String query = "CREATE DATABASE \"Abonne\"\n" + 
				"    WITH \n" + 
				"    TABLESPACE = pg_default\n" + 
				"    CONNECTION LIMIT = -1;";

		String queryCreate =
				"CREATE TABLE public.abonne\n"+
						"(\n"+
						"   \"IMAC\" text,\n" +
						"  \"Nom\" text,\n" +
						"\"Prenom\" text,\n"+
						"\"Email\" text,\n" +
						"\"Sms\" text\n" +
						")\n" +
						"WITH (\n" +
						"OIDS = FALSE\n"+
						");\n" +
						"ALTER TABLE public.abonne\n" +
						"    OWNER to "+user+";";
		
		String loginEntered = (String) request.getParameter("login");
		System.out.println(loginEntered);
		String passwordEntered = (String) request.getParameter("password");
		System.out.println(passwordEntered);

		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String url = "jdbc:postgresql://localhost/";

		

		System.out.println("debut");

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.getMessage();
		}
		System.out.println("lets");
		
		try {
			conn = DriverManager.getConnection(url, user, password);
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
		} catch (SQLException e) {
			System.out.println("JE CREEE DB" + e.getMessage());
		}
		try {
			url= "jdbc:postgresql://localhost:5432/Abonne";
			conn = DriverManager.getConnection(url, user, password);
			pst = conn.prepareStatement(queryCreate);
			rs = pst.executeQuery();
		} catch (SQLException e) {
			System.out.println("JE cree la table " + e.getMessage());
		}
		
		try {
			url= "jdbc:postgresql://localhost:5432/Abonne";
			conn = DriverManager.getConnection(url, user, password);
			System.out.println("connexion ok !");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("go");
		String select = "SELECT * FROM public.abonne\n";
		ListDevices liste = null;
		try {
			pst = conn.prepareStatement(select);
			rs = pst.executeQuery();
			liste = new ListDevices();
			while ( rs.next() ) {
				String imac = rs.getString( "IMAC" );
				System.out.println(imac);
				String email = rs.getString( "Email" );
				System.out.println(email);
				String numTel = rs.getString( "Sms" );
				System.out.println(numTel);
				Device device = new Device(imac,email,numTel);
				System.out.println(device.getImac() + " / " + device.getMail()+ " / " + device.getNum());
				liste.setDevices(device);
			}  	
			for(int i=0; i<liste.getDevices().size();i++) {
				System.out.println("ListedeviceThread : "+liste.getDevices().get(i).getImac());
			}
		} catch (SQLException e) {
			System.out.println("JAFFICHE LE TOUT " +e.getMessage());
		}
		System.out.println("fin");

		// Si l'un des champs est vide
		if ("".equals(loginEntered) || "".equals(passwordEntered)) {
			request.setAttribute("erreur", "Vous devez remplir les deux champs.");
			// Redirection vers le formulaire form.jsp
			getServletContext().getRequestDispatcher("/index.jsp")
			.forward(request, response);
		}

		// Sinon
		else {
			MyDiscoveryListenerThread test = new MyDiscoveryListenerThread(liste.getDevices());
			Thread t1 = new Thread(test); 
			t1.start();

			ListThread listeT = new ListThread();
			ListThread.removeAll();
			ListThread.setThread(t1);

			System.out.println("threaddeb : "+t1);
			request.setAttribute("thread", t1);
			request.setAttribute("login", loginEntered);
			request.setAttribute("password", passwordEntered);
			// Redirection vers la page hello.jsp
			getServletContext().getRequestDispatcher("/hello.jsp")
			.forward(request, response);
		}
	}
}
