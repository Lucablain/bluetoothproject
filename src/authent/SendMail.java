package authent;

import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMail {

	public static void sendEmail(String to, String prenom)
	{
		final String username = "bluetoothReseau@gmail.com";
		final String password = "Bluetooth123";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("bluetoothReseau@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(to));
			message.setSubject("Hello "+prenom+",");
			message.setText("Welcome to MyShop, now you are client of The Shop \n\n It's an automatic mail, don't reply !! Thank You");

			Transport.send(message);

			System.out.println("Done");

		} 

		catch (MessagingException e) 
		{
			// throw new RuntimeException(e);
			System.out.println("Username or Password are incorrect ... exiting ! + "+ e.toString());
		}
	}

	public static void sendEmailPromo(String to)
	{
		final String username = "bluetoothReseau@gmail.com";
		final String password = "Bluetooth123";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("bluetoothReseau@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(to));
			message.setSubject("Promos de la semaine !");
			message.setText("Voici toutes les promos de la semaine :\n � B�b� gar�on : 5%\n � B�b� fille :5%\n� Gar�on 2-10 ans :5%\n� Fille 2-10 ans : 15%\n� Gar�on 10-16 ans : 10% \n� Fille 10-16 ans : 5%  \n\n It's an automatic mail, don't reply !! Thank You");

			Transport.send(message);

			System.out.println("Done");

		} 

		catch (MessagingException e) 
		{
			// throw new RuntimeException(e);
			System.out.println("Username or Password are incorrect ... exiting ! + "+ e.toString());
		}
	}

}