package authent;

import java.io.IOException;
import java.util.ArrayList;

import javax.bluetooth.DataElement;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.util.EncodingUtil;

public class MyDiscoveryListenerThread implements DiscoveryListener, Runnable{
    
    private static Object lock=new Object();
    public ArrayList<RemoteDevice> devices;
    public ArrayList<Device> listeDev;
    private String accessToken = "6UBsniX1f9WJl96IgOaCRSqyYKknPkIG";
	private String BASE_URL = "https://api.smsmode.com/http/1.6/";
    
    public MyDiscoveryListenerThread(ArrayList<Device> listeD) {
        devices = new ArrayList<RemoteDevice>();
        listeDev=listeD;
    }

    public void run() {
    	MyDiscoveryListenerThread listener =  new MyDiscoveryListenerThread(listeDev);
    	
        while(true) {
        	try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        try{
	            LocalDevice localDevice = LocalDevice.getLocalDevice();
	            DiscoveryAgent agent = localDevice.getDiscoveryAgent();
	            agent.startInquiry(DiscoveryAgent.GIAC, (MyDiscoveryListenerThread) listener);
	            
	            try {
	                synchronized(lock){
	                    lock.wait();
	                }
	            }
	            catch (InterruptedException e) {
	                e.printStackTrace();
	            }
	            
	            
	            System.out.println("Device Inquiry Completed. ");
	            
	       
	            UUID[] uuidSet = new UUID[1];
	            uuidSet[0]=new UUID(0x1105); //OBEX Object Push service
	            
	            int[] attrIDs =  new int[] {
	                    0x0100 // Service name
	            };
	            
	            System.out.println("toto ");
	            System.out.println(listener.devices);
	            
	            for (RemoteDevice device : listener.devices) {
	                agent.searchServices(
	                        attrIDs,uuidSet,device,(DiscoveryListener) listener);
	                
	                
	                try {
	                    synchronized(lock){
	                        lock.wait();
	                    }
	                }
	                catch (InterruptedException e) {
	                    e.printStackTrace();
	                }
	                
	                
	                System.out.println("Service search finished.");
	            }
	            
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
        }
    }

    public void deviceDiscovered(RemoteDevice btDevice, DeviceClass arg1) {
        String name;
        try {
            name = btDevice.getFriendlyName(false);
        } catch (Exception e) {
            name = btDevice.getBluetoothAddress();
        }
        for(int i=0;i<devices.size();i++) {
        	if(!devices.get(i).getBluetoothAddress().equals(btDevice.getBluetoothAddress())) {
                devices.add(btDevice);
        	}
        }
        System.out.println("device found: " + name);
        System.out.println("|-> Adress MAC Device : " + btDevice.getBluetoothAddress());
        for(int i=0;i<listeDev.size();i++) {
        	System.out.println(listeDev.get(i).getImac());
        	if(listeDev.get(i).getImac().equals(btDevice.getBluetoothAddress())) {
        		//System.out.println(btDevice.getBluetoothAddress());
        		SendMail.sendEmailPromo(listeDev.get(i).getMail());
        		System.out.println("Debut sms");
        		try {
        			System.out.println("tttttttttttttt");
            		//sendSMSPromo(listeDev.get(i).getNum());
        		}catch(Exception e) {
        			e.printStackTrace();
        		}
        		listeDev.remove(i);
        		System.out.println("After sms");
        	}
        }
    }

    public void inquiryCompleted(int arg0) {
        synchronized(lock){
            lock.notify();
        }
    }

    public void serviceSearchCompleted(int arg0, int arg1) {
        synchronized (lock) {
            lock.notify();
        }
    }

    public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
        for (int i = 0; i < servRecord.length; i++) {
            String url = servRecord[i].getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
            if (url == null) {
                continue;
            }
            DataElement serviceName = servRecord[i].getAttributeValue(0x0100);
            if (serviceName != null) {
            	System.out.println("service " + serviceName.getValue() + " found " + url);
                
                if(serviceName.getValue().equals("OBEX Object Push")){
                    //sendMessageToDevice(url);                
                }
            } else {
                System.out.println("service found " + url);
            }
            
          
        }
    }
    
    public void sendSMSUsingGet(String message, String destinataires, String emetteur, String optionStop) {
		try {
			String getURL = BASE_URL + "sendSMS.do";
			GetMethod httpMethod = new GetMethod(getURL);
			httpMethod.addRequestHeader("Content-Type", "plain/text; charset=ISO-8859-15");

			NameValuePair params[] = { new NameValuePair("accessToken", this.accessToken), //
					new NameValuePair("message", message), //
					new NameValuePair("numero", destinataires), //Receivers separated by a comma
					new NameValuePair("emetteur", emetteur), //Optional parameter, allows to deal with the sms sender
					new NameValuePair("stop", optionStop)}; //Deal with the STOP sms when marketing send (cf. API HTTP documentation) };

			httpMethod.setQueryString(EncodingUtil.formUrlEncode(params, "ISO-8859-15"));

			System.out.println(httpMethod.getURI() + "" + httpMethod.getQueryString());

			executeMethod(httpMethod);
		} catch (Exception e) {
			manageError(e);
		}
	}
	
	public void sendSMSPromo(String num)
	{
		System.out.println("sms ok");
		String	message = "Voici toutes les promos de la semaine :\n � B�b� gar�on : 5%\n � B�b� fille :5%\n� Gar�on 2-10 ans :5%\n� Fille 2-10 ans : 15%\n� Gar�on 10-16 ans : 10% \n� Fille 10-16 ans : 5%  \n\n It's an automatic mail, don't reply !! Thank You";
		String destinataires = num; //Receivers separated by a comma
		String emetteur = "BluetoothProject"; //Optional parameter, allows to deal with the sms sender
		String stopOption = "1"; //Deal with the STOP sms when marketing send (cf. API HTTP documentation)

		sendSMSUsingGet(message, destinataires, emetteur, stopOption);
	}
	private void executeMethod(HttpMethod httpMethod) throws IOException, HttpException {
		HttpClient httpClient = new HttpClient();

		System.out.println(httpMethod);
		int codeReponse = httpClient.executeMethod(httpMethod);
		verifyReponse(httpMethod, codeReponse);
	}

	private void verifyReponse(HttpMethod httpMethod, int codeReponse) throws IOException {
		if (codeReponse == HttpStatus.SC_OK || codeReponse == HttpStatus.SC_ACCEPTED) {
			String result = new String(httpMethod.getResponseBody());
			System.out.println(result);
		}
	}

	private void manageError(Exception e) {
		e.printStackTrace();
		System.err.println("Error during API call");
	}

	public void sendSMSUsingPost(String text, String destinataires, String emetteur, String optionStop) {
		try {
			String postURL = BASE_URL + "sendSMS.do";
			PostMethod httpMethod = new PostMethod(postURL);
			httpMethod.addRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=ISO-8859-15");

			NameValuePair data[] = { new NameValuePair("accessToken", this.accessToken), //
					new NameValuePair("message", text), //
					new NameValuePair("numero", destinataires), //Receivers separated by a comma
					new NameValuePair("emetteur", emetteur), //Optional parameter, allows to deal with the sms sender
					new NameValuePair("stop", optionStop) //Deal with the STOP sms when marketing send (cf. API HTTP documentation)
			};
			httpMethod.setRequestBody(data);

			System.out.println("///////////////////////");
			httpMethod.getRequestEntity().writeRequest(System.out);
			executeMethod(httpMethod);

		} catch (Exception e) {
			manageError(e);
		}
	}
    
    /*private static void sendMessageToDevice(String serverURL){
        try{
            System.out.println("Connecting to " + serverURL);
    
            ClientSession clientSession = (ClientSession) Connector.open(serverURL);
            HeaderSet hsConnectReply = clientSession.connect(null);
            if (hsConnectReply.getResponseCode() != ResponseCodes.OBEX_HTTP_OK) {
                System.out.println("Failed to connect");
                return;
            }
    
            HeaderSet hsOperation = clientSession.createHeaderSet();
            hsOperation.setHeader(HeaderSet.NAME, "Hello.txt");
            hsOperation.setHeader(HeaderSet.TYPE, "text");
    
            //Create PUT Operation
            Operation putOperation = clientSession.put(hsOperation);
    
            // Send some text to server
            byte data[] = "Hello World !!!".getBytes("iso-8859-1");
            OutputStream os = putOperation.openOutputStream();
            os.write(data);
            os.close();
    
            putOperation.close();
    
            clientSession.disconnect(null);
    
            clientSession.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
*/

}
