<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" type="text/css" href="CSS/style.css">
<title>Nouveau Client</title>
</head>
<body>
	<h1 style="text-align:center;">Recherche nouveau client :</h1>
	<%
      // Récupération du message d'erreur 
      String erreur = (String) request.getAttribute("erreur");
      // Affichage du message s'il existe
      if (erreur != null) { %>
            <strong style="background:white;margin-left:37%;color:red;">Erreur : Vous n'avez pas rempli tous les champs.</strong>
      <%
      } %>
	<form method="post" action="searchClient">
		<label>Lancer la recherche de téléphone :</label><br><br>
		<input name="Valider" type="submit" onclick="document.getElementById('loader').style.display='block';"/>
	</form>
	<img id="loader" style="display:none;position:fixed;left:500px;top:200px;" src="CSS/loading.gif" alt="load" height="500" width="500">
</body>
</html>