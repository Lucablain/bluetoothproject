<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" type="text/css" href="CSS/style.css">
<title>Ajout du client</title>
</head>
<body>
	<h1>Ajout du client dans la base de donnee :</h1>
	<form action="ajoutBDDMAIL" method="post">
    <div>
        <label for="nom">Nom :</label>
        <input type="text" id="nom" name="nom">
    </div>
    <div>
        <label for="prenom">Prenom :</label>
        <input type="text" id="prenom" name="prenom">
    </div>
    <div>
        <label for="email">E-mail :</label>
        <input type="email" id="email" name="email">
    </div>
        <div>
        <label for="num">Numero Tel :</label>
        <input type="text" id="num" name="num">
    </div>
    <div>
        <label for="MAC">Adresse Mac :</label>
        <textarea id="MAC" name="MAC"><%= request.getAttribute("adressMAC") %></textarea>
    </div>
    <input type="submit" value="Valider le formulaire et Envoyer un mail"/>
</form>
</body>
</html>