package authent;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class Login extends HttpServlet {
      private static final long serialVersionUID = 1L;
 
      public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      }
 
      public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            // R�cup�rer les donn�es re�ues du formulaire
            String loginEntered = (String) request.getParameter("login");
            System.out.println(loginEntered);
            String passwordEntered = (String) request.getParameter("password");
            System.out.println(passwordEntered);
            
            // Si l'un des champs est vide
            if ("".equals(loginEntered) || "".equals(passwordEntered)) {
                  request.setAttribute("erreur", "Vous devez remplir les deux champs.");
                  // Redirection vers le formulaire form.jsp
                  getServletContext().getRequestDispatcher("/index.jsp")
                             .forward(request, response);
            }
 
            // Sinon
            else {
                  request.setAttribute("login", loginEntered);
                  request.setAttribute("password", passwordEntered);
                  // Redirection vers la page hello.jsp
                  getServletContext().getRequestDispatcher("/hello.jsp")
                             .forward(request, response);
            }
      }
}
 