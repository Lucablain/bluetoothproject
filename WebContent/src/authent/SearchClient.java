package authent;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.RemoteDevice;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class SearchClient extends HttpServlet {
      private static final long serialVersionUID = 1L;
 
      public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      }
 
      public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            // R�cup�rer les donn�es re�ues du formulaire
            String nameTel = (String) request.getParameter("nameTel");
            System.out.println(nameTel);
            
            
            // Si l'un des champs est vide
            if ("".equals(nameTel)) {
                  request.setAttribute("erreur", "Vous devez remplir le champ.");
                  // Redirection vers le formulaire form.jsp
                  getServletContext().getRequestDispatcher("/newClient.jsp")
                             .forward(request, response);
            }
 
            // Sinon
            else {
                  request.setAttribute("nameTel", nameTel);
                  DiscoveryListener test;
                  MyDiscoveryListener searchTel = new MyDiscoveryListener(); 
              	try {
					Thread.sleep(30000);
					List<RemoteDevice> devices = searchTel.devices;
					request.setAttribute("listNewClient", devices);

	                  //Redirection vers la page hello.jsp
	                  getServletContext().getRequestDispatcher("/listNewClient.jsp")
	                             .forward(request, response);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
              	
            }
      }
}
 