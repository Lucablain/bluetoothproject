<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" type="text/css" href="CSS/style.css">
<title>FiDelity</title>
</head>
<body>
	<h1>F<span style="color:lightgray;">iD</span>elity</h1><hr/>
	
	 <%
      // Récupération du message d'erreur 
      String erreur = (String) request.getAttribute("erreur");
      // Affichage du message s'il existe
      if (erreur != null) { %>
            <strong style="background:white;margin-left:37%;color:red;">Erreur : Vous n'avez pas rempli tous les champs.</strong>
      <%
      } %>
	
	<form method="post" action="login">
	<label>Login :</label> <input name="login" type="text" /><br><br>
	<label>Mot de passe :</label> <input name="password" type="password" /><br><br>
	<input name="Valider" type="submit" />
	</form>
</body>
</html>